From 7ab0b3407bd361a5cbf992958f40772a8ab5ea9d Mon Sep 17 00:00:00 2001
From: Greg Hudson <ghudson@mit.edu>
Date: Sun, 30 Jul 2023 01:07:38 -0400
Subject: [PATCH] Get rid of pkinit_crypto_openssl.h

Fold pkinit_crypto_openssl.h into the one source file where it was
used.  Also clean up the include of <arpa/inet.h>, as htonl() is no
longer used after commit 1c87ce6c44a9de0824580a2d72a8a202237e01f4.

(cherry picked from commit b3352945fb8836f8b4095e0b8aad04b54aca3152)
---
 src/plugins/preauth/pkinit/deps               |   2 +-
 .../preauth/pkinit/pkinit_crypto_openssl.c    |  85 +++++++++++-
 .../preauth/pkinit/pkinit_crypto_openssl.h    | 121 ------------------
 3 files changed, 83 insertions(+), 125 deletions(-)
 delete mode 100644 src/plugins/preauth/pkinit/pkinit_crypto_openssl.h

diff --git a/src/plugins/preauth/pkinit/deps b/src/plugins/preauth/pkinit/deps
index 58320aa801..b6f4476fe8 100644
--- a/src/plugins/preauth/pkinit/deps
+++ b/src/plugins/preauth/pkinit/deps
@@ -112,4 +112,4 @@ pkinit_crypto_openssl.so pkinit_crypto_openssl.po $(OUTPRE)pkinit_crypto_openssl
   $(top_srcdir)/include/krb5/plugin.h $(top_srcdir)/include/krb5/preauth_plugin.h \
   $(top_srcdir)/include/port-sockets.h $(top_srcdir)/include/socket-utils.h \
   pkcs11.h pkinit.h pkinit_accessor.h pkinit_crypto.h \
-  pkinit_crypto_openssl.c pkinit_crypto_openssl.h pkinit_trace.h
+  pkinit_crypto_openssl.c pkinit_trace.h
diff --git a/src/plugins/preauth/pkinit/pkinit_crypto_openssl.c b/src/plugins/preauth/pkinit/pkinit_crypto_openssl.c
index f6d494bd11..ae8599d5a2 100644
--- a/src/plugins/preauth/pkinit/pkinit_crypto_openssl.c
+++ b/src/plugins/preauth/pkinit/pkinit_crypto_openssl.c
@@ -30,20 +30,99 @@
  */
 
 #include "k5-int.h"
-#include "pkinit_crypto_openssl.h"
 #include "k5-buf.h"
 #include "k5-err.h"
 #include "k5-hex.h"
-#include <unistd.h>
+#include "pkinit.h"
 #include <dirent.h>
-#include <arpa/inet.h>
 
+#include <openssl/bn.h>
+#include <openssl/dh.h>
+#include <openssl/x509.h>
+#include <openssl/pkcs7.h>
+#include <openssl/pkcs12.h>
+#include <openssl/obj_mac.h>
+#include <openssl/x509v3.h>
+#include <openssl/err.h>
+#include <openssl/evp.h>
+#include <openssl/sha.h>
+#include <openssl/asn1.h>
+#include <openssl/pem.h>
+#include <openssl/asn1t.h>
+#include <openssl/cms.h>
 #if OPENSSL_VERSION_NUMBER >= 0x30000000L
 #include <openssl/core_names.h>
 #include <openssl/kdf.h>
+#include <openssl/decoder.h>
 #include <openssl/params.h>
 #endif
 
+#define DN_BUF_LEN  256
+#define MAX_CREDS_ALLOWED 20
+
+struct _pkinit_cred_info {
+    char *name;
+    X509 *cert;
+    EVP_PKEY *key;
+#ifndef WITHOUT_PKCS11
+    CK_BYTE_PTR cert_id;
+    int cert_id_len;
+#endif
+};
+typedef struct _pkinit_cred_info *pkinit_cred_info;
+
+struct _pkinit_identity_crypto_context {
+    pkinit_cred_info creds[MAX_CREDS_ALLOWED+1];
+    STACK_OF(X509) *my_certs;   /* available user certs */
+    char *identity;             /* identity name for user cert */
+    int cert_index;             /* cert to use out of available certs*/
+    EVP_PKEY *my_key;           /* available user keys if in filesystem */
+    STACK_OF(X509) *trustedCAs; /* available trusted ca certs */
+    STACK_OF(X509) *intermediateCAs;   /* available intermediate ca certs */
+    STACK_OF(X509_CRL) *revoked;    /* available crls */
+    int pkcs11_method;
+    krb5_prompter_fct prompter;
+    void *prompter_data;
+#ifndef WITHOUT_PKCS11
+    char *p11_module_name;
+    CK_SLOT_ID slotid;
+    char *token_label;
+    char *cert_label;
+    /* These are crypto-specific. */
+    struct plugin_file_handle *p11_module;
+    CK_SESSION_HANDLE session;
+    CK_FUNCTION_LIST_PTR p11;
+    uint8_t *cert_id;
+    size_t cert_id_len;
+    CK_MECHANISM_TYPE mech;
+#endif
+    krb5_boolean defer_id_prompt;
+    pkinit_deferred_id *deferred_ids;
+};
+
+struct _pkinit_plg_crypto_context {
+    EVP_PKEY *dh_1024;
+    EVP_PKEY *dh_2048;
+    EVP_PKEY *dh_4096;
+    EVP_PKEY *ec_p256;
+    EVP_PKEY *ec_p384;
+    EVP_PKEY *ec_p521;
+    ASN1_OBJECT *id_pkinit_authData;
+    ASN1_OBJECT *id_pkinit_DHKeyData;
+    ASN1_OBJECT *id_pkinit_rkeyData;
+    ASN1_OBJECT *id_pkinit_san;
+    ASN1_OBJECT *id_ms_san_upn;
+    ASN1_OBJECT *id_pkinit_KPClientAuth;
+    ASN1_OBJECT *id_pkinit_KPKdc;
+    ASN1_OBJECT *id_ms_kp_sc_logon;
+    ASN1_OBJECT *id_kp_serverAuth;
+};
+
+struct _pkinit_req_crypto_context {
+    X509 *received_cert;
+    EVP_PKEY *client_pkey;
+};
+
 static krb5_error_code pkinit_init_pkinit_oids(pkinit_plg_crypto_context );
 static void pkinit_fini_pkinit_oids(pkinit_plg_crypto_context );
 
diff --git a/src/plugins/preauth/pkinit/pkinit_crypto_openssl.h b/src/plugins/preauth/pkinit/pkinit_crypto_openssl.h
deleted file mode 100644
index b7a3358800..0000000000
--- a/src/plugins/preauth/pkinit/pkinit_crypto_openssl.h
+++ /dev/null
@@ -1,121 +0,0 @@
-/*
- * COPYRIGHT (C) 2006,2007
- * THE REGENTS OF THE UNIVERSITY OF MICHIGAN
- * ALL RIGHTS RESERVED
- *
- * Permission is granted to use, copy, create derivative works
- * and redistribute this software and such derivative works
- * for any purpose, so long as the name of The University of
- * Michigan is not used in any advertising or publicity
- * pertaining to the use of distribution of this software
- * without specific, written prior authorization.  If the
- * above copyright notice or any other identification of the
- * University of Michigan is included in any copy of any
- * portion of this software, then the disclaimer below must
- * also be included.
- *
- * THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION
- * FROM THE UNIVERSITY OF MICHIGAN AS TO ITS FITNESS FOR ANY
- * PURPOSE, AND WITHOUT WARRANTY BY THE UNIVERSITY OF
- * MICHIGAN OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
- * WITHOUT LIMITATION THE IMPLIED WARRANTIES OF
- * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE
- * REGENTS OF THE UNIVERSITY OF MICHIGAN SHALL NOT BE LIABLE
- * FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
- * CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING
- * OUT OF OR IN CONNECTION WITH THE USE OF THE SOFTWARE, EVEN
- * IF IT HAS BEEN OR IS HEREAFTER ADVISED OF THE POSSIBILITY OF
- * SUCH DAMAGES.
- */
-
-#ifndef _PKINIT_CRYPTO_OPENSSL_H
-#define _PKINIT_CRYPTO_OPENSSL_H
-
-#include "pkinit.h"
-
-#include <openssl/bn.h>
-#include <openssl/dh.h>
-#include <openssl/x509.h>
-#include <openssl/pkcs7.h>
-#include <openssl/pkcs12.h>
-#include <openssl/obj_mac.h>
-#include <openssl/x509v3.h>
-#include <openssl/err.h>
-#include <openssl/evp.h>
-#include <openssl/sha.h>
-#include <openssl/asn1.h>
-#include <openssl/pem.h>
-#include <openssl/asn1t.h>
-#include <openssl/cms.h>
-#if OPENSSL_VERSION_NUMBER >= 0x30000000L
-#include <openssl/core_names.h>
-#include <openssl/decoder.h>
-#endif
-
-#define DN_BUF_LEN  256
-#define MAX_CREDS_ALLOWED 20
-
-struct _pkinit_cred_info {
-    char *name;
-    X509 *cert;
-    EVP_PKEY *key;
-#ifndef WITHOUT_PKCS11
-    CK_BYTE_PTR cert_id;
-    int cert_id_len;
-#endif
-};
-typedef struct _pkinit_cred_info * pkinit_cred_info;
-
-struct _pkinit_identity_crypto_context {
-    pkinit_cred_info creds[MAX_CREDS_ALLOWED+1];
-    STACK_OF(X509) *my_certs;   /* available user certs */
-    char *identity;             /* identity name for user cert */
-    int cert_index;             /* cert to use out of available certs*/
-    EVP_PKEY *my_key;           /* available user keys if in filesystem */
-    STACK_OF(X509) *trustedCAs; /* available trusted ca certs */
-    STACK_OF(X509) *intermediateCAs;   /* available intermediate ca certs */
-    STACK_OF(X509_CRL) *revoked;    /* available crls */
-    int pkcs11_method;
-    krb5_prompter_fct prompter;
-    void *prompter_data;
-#ifndef WITHOUT_PKCS11
-    char *p11_module_name;
-    CK_SLOT_ID slotid;
-    char *token_label;
-    char *cert_label;
-    /* These are crypto-specific */
-    struct plugin_file_handle *p11_module;
-    CK_SESSION_HANDLE session;
-    CK_FUNCTION_LIST_PTR p11;
-    uint8_t *cert_id;
-    size_t cert_id_len;
-    CK_MECHANISM_TYPE mech;
-#endif
-    krb5_boolean defer_id_prompt;
-    pkinit_deferred_id *deferred_ids;
-};
-
-struct _pkinit_plg_crypto_context {
-    EVP_PKEY *dh_1024;
-    EVP_PKEY *dh_2048;
-    EVP_PKEY *dh_4096;
-    EVP_PKEY *ec_p256;
-    EVP_PKEY *ec_p384;
-    EVP_PKEY *ec_p521;
-    ASN1_OBJECT *id_pkinit_authData;
-    ASN1_OBJECT *id_pkinit_DHKeyData;
-    ASN1_OBJECT *id_pkinit_rkeyData;
-    ASN1_OBJECT *id_pkinit_san;
-    ASN1_OBJECT *id_ms_san_upn;
-    ASN1_OBJECT *id_pkinit_KPClientAuth;
-    ASN1_OBJECT *id_pkinit_KPKdc;
-    ASN1_OBJECT *id_ms_kp_sc_logon;
-    ASN1_OBJECT *id_kp_serverAuth;
-};
-
-struct _pkinit_req_crypto_context {
-    X509 *received_cert;
-    EVP_PKEY *client_pkey;
-};
-
-#endif	/* _PKINIT_CRYPTO_OPENSSL_H */
-- 
2.47.1

